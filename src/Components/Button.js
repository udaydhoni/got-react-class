
function Button (props) {
    console.log(props);
    return (
        <div className="buttons">
            {
                props.houses.map((family)=>{
                    if (props.styles[family['name']]) {
                        return <button onClick={props.listener} className = 'active-button'>{family['name']}</button>
                    }
                    else {
                        return <button onClick={props.listener}>{family['name']}</button>
                    }
                }
                )
            }
        </div>
        
    ) 
}

export default Button